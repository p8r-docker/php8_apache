FROM php:8.0.2-apache-buster

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN a2enmod rewrite && \
    service apache2 restart

RUN docker-php-ext-install mysqli && \
    docker-php-ext-install pdo_mysql 

RUN install-php-extensions gd xdebug bcmath calendar mongodb

COPY ./html/* /var/www/html/


